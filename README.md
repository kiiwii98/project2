# Revature Social Network

## Project Description 

In this social network, everyone is friends with everyone. As a user, you will be able to register and login to your account. When you successfully login, you are automatically friends with everyone which means you will see a feed of everyone's posts. Users will be able to create a post and like other people's posts. Users will also have the ability to view other profiles which will display posts of that specific user.

## Tecnologies Used

* Java - version 11
* Spring Boot - version 2.6.7
* Spring Framework - version 2.2.6
* Amazon Web Services

## Features

* HTML pages set up
* Login functionality
* Posts (text, images, videos)
* Reset password
* Password hashing 
* Logout functionality

## To-Do

* Connecting front-end with the back-end 
    * javascript
    * DOM manipulation 

## Usage

A user can create thier account. Users are automatically friends with all other users. A user can see a global feed with other users' posts. 
A user can also post (text, video, image). A user can edit their profile page. 

## Contributors

* Christian Castro
* Colin Modderman
* Kendrickp Garcia

## Project status
App is still under development so nothing has been pushed directly to the main branch as of yet. 
